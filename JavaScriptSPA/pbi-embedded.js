// import * as pbi from 'powerbi-client';
// import * as pbi from 'powerbi-client';

/**
 * Embed a report by calling embedPbiReport
 * Best practices @see https://docs.microsoft.com/en-us/power-bi/developer/embedded-performance-best-practices
 */
const pbiEmbedded = {
  config: {
    getAccessTokenUrl: 'http://localhost:30662/accessToken',
    workspaceId: 'a9a52ba9-7961-482f-8dc4-6936c46c8bcd',
    reportId: '55c4676b-8c2c-4b98-b12e-e6e015062487',
    username: 'powerbiembed@expertime.com',
    password: 'Zuvo6377',
    loggedInUsername: null,
    reportContainer: document.getElementById('container')
  },
  /**
   * Get an Access token by fetching node server URL
   * Instead of using node server for getting access token,you can use an Azure Function
   * @see https://www.taygan.co/blog/2018/05/14/embedded-analytics-with-power-bi
   * Why/How should you get an Access token @see https://docs.microsoft.com/en-us/power-bi/developer/get-azuread-access-token
   */
  async getAccessToken() {
    const response = await fetch(this.config.getAccessTokenUrl);
    const accessToken = await response.json();
    return accessToken.accessToken;
  },
  /**
   * Get embed token by sending a HTTP POST request to PowerBI Api
   * @param {string} accessToken
   * @param {string} username
   */
  async getEmbedToken(accessToken, username) {
    // Configure request Headers
    const headers = new Headers();
    headers.append('Authorization', 'Bearer ' + accessToken);
    headers.append('Content-Type', 'application/json');

    /**
     * Report body configuration
     * @see https://docs.microsoft.com/en-us/rest/api/power-bi/embedtoken/reports_generatetokeningroup#request-body
     * Configure RLS @see https://docs.microsoft.com/en-us/power-bi/developer/embedded-row-level-security#applying-user-and-role-to-an-embed-token
     */
    const requestBody = {
      accessLevel: 'View',
      identities: [
        {
          username,
          roles: ['Username'], // the role name your created for your report
          // datasets: ['0de816e9-8862-461e-8dc0-a767cb1e4681'] // get it by calling it's api https://docs.microsoft.com/en-us/rest/api/power-bi/datasets/getdatasetsingroup
          datasets: ['b904234a-96d1-42c1-a994-19b8f7ada91c'] // get it by calling it's api https://docs.microsoft.com/en-us/rest/api/power-bi/datasets/getdatasetsingroup
        }
      ]
    };

    const { workspaceId, reportId } = this.config;

    // Configure request options
    const options = {
      method: 'POST',
      headers,
      body: JSON.stringify(requestBody)
    };

    // Send request to Power Api
    const response = await fetch(
      `https://api.powerbi.com/v1.0/myorg/groups/${workspaceId}/reports/${reportId}/GenerateToken`,
      options
    );

    const embedTokenObj = await response.json();

    return embedTokenObj.token;
  },
  /**
   * [?] Should respond to storage change
   * @see https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API#Responding_to_storage_changes_with_the_StorageEvent
   */
  async getLoggedInUsername() {
    const loggedInUser = localStorage.getItem('persist:root');

    if (loggedInUser !== null) {
      return JSON.parse(loggedInUser).username;
    }

    return false;
  },
  /**
   * @param {Node} reportContainer The element where the report is appended
   */
  async embedPbiReport(reportContainer) {
    const accessTokenPromise = this.getAccessToken();
    const loggedInUsernamePromise = this.getLoggedInUsername();
    
    // Await for accessToken and loggedInUser to resolve
    const [accessToken, loggedInUsername] = await Promise.all([
      accessTokenPromise,
      loggedInUsernamePromise
    ]);


    // Use accessToken and loggedInUser (RLS) to get embed token
    const embedToken = await this.getEmbedToken(accessToken, loggedInUsername);

    // Finally, embed pbi report by using embed token
    const models = window['powerbi-client'].models;
    
    const { reportId, workspaceId } = this.config;

    /**
     * Configure the embedded report
     * @see https://github.com/Microsoft/PowerBI-JavaScript/wiki/Embed-Configuration-Details
     */
    const embedConfig = {
      type: 'report',
      id: this.config.reportId,
      embedUrl: `https://app.powerbi.com/reportEmbed?reportId=${reportId}&groupId=${workspaceId}`,
      tokenType: models.TokenType.Embed,
      accessToken: embedToken,
      // settings: {
        // layoutType: models.LayoutType.Custom,
        // customLayout: {
        //   pageSize: {
        //     type: models.PageSizeType.Custom,
        //     width: 1000,
        //     height: 500
        //   },
        //   displayOption: models.DisplayOption.FitToPage
        // },
      //   filterPaneEnabled: false,
      //   navContentPaneEnabled: false
      // }
    };

    // Dipslay report
    powerbi.embed(reportContainer, embedConfig);

    // [ ] Add fullscreen btn
  },
  toggleFullScreen() {
    const report = powerbi.get(this.config.reportContainer);
    report.fullScreen();
  }
};

// TESTING : setting username
localStorage.setItem(
  'persist:root',
  JSON.stringify({ username: 'clovis.ravion@argon-consult.com' })
);

const reportContainer = document.getElementById('container');

pbiEmbedded.embedPbiReport(reportContainer);