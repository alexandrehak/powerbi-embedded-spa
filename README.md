## Power BI Embedding SPA

These instructions will get you running on your local machine for development and testing purposes.
It explains how to embed a Power BI Report in a web page for Application owns data model.

## Description

The application is based on the getting started Javascript SPA code base [Sign in users and call the Microsoft Graph API from a JavaScript single-page application (SPA)](https://docs.microsoft.com/en-us/azure/active-directory/develop/tutorial-v2-javascript-spa)

server.js
- Added function getAccessToken
- Added /accessToken url

index.html
- Remove script tag

pbi-embedded.js
- Added pbiEmbedded object

## Prerequisites

- A Power BI Report to be embedded
- Application registered
- Azure Subscription
- "Default client type" is turned on (app portal)
- Check API permissions for Power BI Service (app portal)
- Check Grant Consent (app portal)

## Start

Install dependencies

```
npm i
```

Configure in pbi-embedded.js

```javascript
config: {
    getAccessTokenUrl: 'http://localhost:30662/accessToken',
    workspaceId: 'Find it in the report URL as groups',
    reportId: 'Also in the report URL',
    username: 'The report/app-admin owner email',
    password: '',
    loggedInUsername: null,
    reportContainer: "e.g. document.getElementById('container')"
}
```

Run the application

```
npm start
```

Display a report by calling

```javascript
pbiEmbedded.embedPbiReport(reportContainer);
```

Configure your report by editing
```javascript
const embedConfig = {
      type: 'report',
      id: this.config.reportId,
      embedUrl: `https://app.powerbi.com/reportEmbed?reportId=${reportId}&groupId=${workspaceId}`,
      tokenType: models.TokenType.Embed,
      accessToken: embedToken,
      settings: {
        // Configure here
        filterPaneEnabled: false,
        navContentPaneEnabled: false
      }
    };
```

## Documention used

- [Embedded analytics with Power BI](https://docs.microsoft.com/en-us/power-bi/developer/embedding)
- [Power BI Embedded Playground](https://microsoft.github.io/PowerBI-JavaScript/demo/v2-demo/index.html)
- [Embed Token - Reports GenerateTokenInGroup](https://docs.microsoft.com/en-us/rest/api/power-bi/embedtoken/reports_generatetokeningroup#tokenaccesslevel)
- [Microsoft identity platform and the OAuth 2.0 resource owner password credential](https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth-ropc)
- [How To: Get embed token using Get/Post only](https://community.powerbi.com/t5/Developer/How-To-Get-embed-token-using-Get-Post-only/m-p/294475)
- [Sign in users and call the Microsoft Graph API from a JavaScript single-page application (SPA)](https://docs.microsoft.com/en-us/azure/active-directory/develop/tutorial-v2-javascript-spa)