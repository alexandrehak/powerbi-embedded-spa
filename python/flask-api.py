import sys
import json
import logging
import requests
import msal
import flask
from flask import request, jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True

config = {
    "authority": "https://login.microsoftonline.com/organizations",
    "client_id": "dd38dafb-c9f3-4c0e-9b1e-738f92775f6f",
    "username": "powerbi@safecube.com",
    "password": "Wuq63896",
    "scope": ["https://analysis.windows.net/powerbi/api/.default"],
    "endpoint": "https://api.powerbi.com/v1.0/myorg/groups/81409500-1c79-451a-b6ee-a6e1717036e3/reports/b7b9822f-c420-4559-a5e7-f0f81dd794e8/GenerateToken"
}

# A route to return all of the available entries in our catalog.

@app.route('/get-embed-token', methods=['GET', 'POST'])
def getEmbedToken():
    print('-------- received a request --------')
    print(request.args.get('username'))
    # Create a preferably long-lived app instance which maintains a token cache.
    pbiApp = msal.PublicClientApplication(
        config["client_id"], authority=config["authority"],
        # token_cache=...  # Default cache is in memory only.
        # You can learn how to use SerializableTokenCache from
        # https://msal-python.rtfd.io/en/latest/#msal.SerializableTokenCache
    )

    # The pattern to acquire a token looks like this.
    result = None
    # Firstly, check the cache to see if this end user has signed in before
    accounts = pbiApp.get_accounts(username=config["username"])

    if accounts:
        logging.info(
            "Account(s) exists in cache, probably with token too. Let's try.")
        result = pbiApp.acquire_token_silent(config["scope"], account=accounts[0])

    if not result:
        logging.info(
            "No suitable token exists in cache. Let's get a new one from AAD.")
        # See this page for constraints of Username Password Flow.
        # https://github.com/AzureAD/microsoft-authentication-library-for-python/wiki/Username-Password-Authentication
        result = pbiApp.acquire_token_by_username_password(
            config["username"], config["password"], scopes=config["scope"])

    if "access_token" in result:
        # [ ] Add RLS (use username)
        # Use token to get embed token
        username = request.args.get('username')

        graph_data = requests.post(
            config["endpoint"],
            headers={
                'Authorization': 'Bearer ' + result['access_token'],
                'Content-Type': 'application/json'
            },
            json={
                'accessLevel': 'View',
                'identities': [
                    {
                        'username': username, # the current user sent by the client
                        'roles': ['Username'], # the role name your created for your report
                        'datasets': ['f96f8d4a-c85a-4b9f-8ed7-abe8912ff3e6'] # get it by calling it's api https://docs.microsoft.com/en-us/rest/api/power-bi/datasets/getdatasetsingroup
                    }
                ]
            }).json()
        # should return this token to client
        # print("PBI embed token call result: %s" % json.dumps(graph_data, indent=2))
        response = flask.jsonify(graph_data)
        response.headers.add('Access-Control-Allow-Origin', '*') # prevent CORS issues
        return response
        # return jsonify({'token': result['access_token']})
    else:
        print(result.get("error"))
        print(result.get("error_description"))
        # You may need this when reporting a bug
        print(result.get("correlation_id"))
        # Not mean to be coded programatically, but...
        if 65001 in result.get("error_codes", []):
            # AAD requires user consent for U/P flow
            print("Visit this to consent:", app.get_authorization_request_url(config["scope"]))
    

app.run()
