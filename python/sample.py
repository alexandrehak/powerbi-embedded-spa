"""
The configuration file would look like this:
{
    "authority": "https://login.microsoftonline.com/organizations",
    "client_id": "your_client_id",
    "username": "your_username@your_tenant.com",
    "password": "This is a sample only. You better NOT persist your password.",
    "scope": ["User.ReadBasic.All"],
        // You can find the other permission names from this document
        // https://docs.microsoft.com/en-us/graph/permissions-reference
    "endpoint": "https://graph.microsoft.com/v1.0/users"
        // You can find more Microsoft Graph API endpoints from Graph Explorer
        // https://developer.microsoft.com/en-us/graph/graph-explorer
}
You can then run this sample with a JSON configuration file:
    python sample.py parameters.json
"""

# For simplicity, we'll read config file from 1st CLI param sys.argv[1]
import sys
import json
import logging
import requests
import msal


# config = json.load(open(sys.argv[1]))

# result = {
#   'access_token': "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkJCOENlRlZxeWFHckdOdWVoSklpTDRkZmp6dyIsImtpZCI6IkJCOENlRlZxeWFHckdOdWVoSklpTDRkZmp6dyJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLndpbmRvd3MubmV0L3Bvd2VyYmkvYXBpIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvNTQ3Yjc5M2UtNTg1Mi00ZjA1LWFlMjQtNzQ0OWY1ZTlkZGIxLyIsImlhdCI6MTU3NjY5MDA4MSwibmJmIjoxNTc2NjkwMDgxLCJleHAiOjE1NzY2OTM5ODEsImFjY3QiOjAsImFjciI6IjEiLCJhaW8iOiJBU1FBMi84TkFBQUFxeDhrODc1dW1ZOHM0L2o1bFVXYmFZNndhbitma1dZR0t1NDN3cEFlNVBZPSIsImFtciI6WyJwd2QiXSwiYXBwaWQiOiI3ZjU5YTc3My0yZWFmLTQyOWMtYTA1OS01MGZjNWJiMjhiNDQiLCJhcHBpZGFjciI6IjIiLCJmYW1pbHlfbmFtZSI6IlBvd2VyQklwcm8iLCJnaXZlbl9uYW1lIjoiQ29tcHRlIiwiaXBhZGRyIjoiODguMTIyLjI1MC42OCIsIm5hbWUiOiJDb21wdGUgUG93ZXJCSXBybyIsIm9pZCI6IjIyOWE0NTk5LTM5YWItNGVkOS05MzFjLTIxNmQwZGQxZjZiNiIsInB1aWQiOiIxMDAzMjAwMDkxMEIyODAyIiwic2NwIjoidXNlcl9pbXBlcnNvbmF0aW9uIiwic3ViIjoiMEkydHpTbWJ3cUUwVmRvekZzQ2d1djFXTFFtZkJ6VEFIRkxLRDdZTGFCWSIsInRpZCI6IjU0N2I3OTNlLTU4NTItNGYwNS1hZTI0LTc0NDlmNWU5ZGRiMSIsInVuaXF1ZV9uYW1lIjoicG93ZXJiaUBzYWZlY3ViZS5jb20iLCJ1cG4iOiJwb3dlcmJpQHNhZmVjdWJlLmNvbSIsInV0aSI6IlVlYXJSUE1oV1VhdzNVOEFIUmRJQUEiLCJ2ZXIiOiIxLjAiLCJ3aWRzIjpbImE5ZWE4OTk2LTEyMmYtNGM3NC05NTIwLThlZGNkMTkyODI2YyJdfQ.NtgWWXzPnGZempYROH7dViZ0EJlQ4MrKEFoVHKF_B1N__RJKlMiVijWcf6AWiNWS5lXf8pnFrKTQ0mZZCzonAR_I6SXmrg52Co8mQBvFGuQgUte01tXtRjlluyMCkKfmuvpA3qMGPDyKpp_RolaIjjmsoQ8AAZkOyBTFogK8oCAJ3uAtFJax1GAoaA7V1qhdf67VcKZFvxYBbBQOmWJjV-vMz8IlGVysMCGbgcjEZ-DVCnRELJAqywfHuNSyRT92z7Xwh3JdIBSZli_f6-Jg61pJ9FV2IP7MxCoXeIr89Wcg949zbv_7lDA4pOIcllX9fdrbWbBUvmCrbipv0GUpxw"
#     # 'access_token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkJCOENlRlZxeWFHckdOdWVoSklpTDRkZmp6dyIsImtpZCI6IkJCOENlRlZxeWFHckdOdWVoSklpTDRkZmp6dyJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLndpbmRvd3MubmV0L3Bvd2VyYmkvYXBpIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvNjQ5NDQ2MGUtODYwMC00ZWRjLTg1MGYtNTI4ZThmYWFkMjkwLyIsImlhdCI6MTU3NjY4ODY1MiwibmJmIjoxNTc2Njg4NjUyLCJleHAiOjE1NzY2OTI1NTIsImFjY3QiOjAsImFjciI6IjEiLCJhaW8iOiJBVFFBeS84TkFBQUE5aUpVVmpmMFcvV0pLcU5IT3hham1haGMzck4vS1ZMK0hRT3VzZkFaTzdIbVQ3dmdKeGd2Q1hFa1Irc2ZXUW9wIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6ImI4NGRhZTg3LTJmN2EtNDM3Ny04NzBkLTYyNDdhOTE2N2E2ZiIsImFwcGlkYWNyIjoiMCIsImlwYWRkciI6Ijg4LjEyMi4yNTAuNjgiLCJuYW1lIjoiUG93ZXJiaWVtYmVkIiwib2lkIjoiNTJlZmJiOWQtYmQxMy00MDFkLThjZmItMzVkMWY4NzM5YTYxIiwicHVpZCI6IjEwMDMyMDAwNzc3OUJCQjEiLCJzY3AiOiJBcHAuUmVhZC5BbGwgQ2FwYWNpdHkuUmVhZC5BbGwgQ2FwYWNpdHkuUmVhZFdyaXRlLkFsbCBDb250ZW50LkNyZWF0ZSBEYXNoYm9hcmQuUmVhZC5BbGwgRGFzaGJvYXJkLlJlYWRXcml0ZS5BbGwgRGF0YWZsb3cuUmVhZC5BbGwgRGF0YWZsb3cuUmVhZFdyaXRlLkFsbCBEYXRhc2V0LlJlYWQuQWxsIERhdGFzZXQuUmVhZFdyaXRlLkFsbCBHYXRld2F5LlJlYWQuQWxsIEdhdGV3YXkuUmVhZFdyaXRlLkFsbCBSZXBvcnQuUmVhZC5BbGwgUmVwb3J0LlJlYWRXcml0ZS5BbGwgU3RvcmFnZUFjY291bnQuUmVhZC5BbGwgU3RvcmFnZUFjY291bnQuUmVhZFdyaXRlLkFsbCBXb3Jrc3BhY2UuUmVhZC5BbGwgV29ya3NwYWNlLlJlYWRXcml0ZS5BbGwiLCJzdWIiOiJ3ekVmUzNZbnUtSnlkQUhJN0U2M0RVejRHT1Y0QjV4TXV5NURibzYzRlRFIiwidGlkIjoiNjQ5NDQ2MGUtODYwMC00ZWRjLTg1MGYtNTI4ZThmYWFkMjkwIiwidW5pcXVlX25hbWUiOiJwb3dlcmJpZW1iZWRAZXhwZXJ0aW1lLmNvbSIsInVwbiI6InBvd2VyYmllbWJlZEBleHBlcnRpbWUuY29tIiwidXRpIjoiX2JwTXRoWDJta3lFTk5UcndMY25BQSIsInZlciI6IjEuMCJ9.cnCX7sDHKF1LkrGA8-fBbXYnfme2HfQpmOndllDYVZGtWpbZ5IMfEtGgkJLGvQP0WlDS2Pga7IrrEutgEWP2M4iPzmGfXBvsve7j6ucRwoWnOfPeCLuWtY5UDbC65-1HfadDgXzzvn6SUqgK593QV4k9kdgxo7r2ET06UGEO_ZOKLMBw72pg87OhRf_QxKhHULiAKl-V3TQt_nTMC4NQj1spQVqDZtq3S0CLtEV1X5l426_fViXyZNxuyXSvRXy1vyG6kuVpov1QRFC24AFi_qhvev6brf1Bnkg-7e9W77KAWOUA8CSA-kXR0XUXRlYdJmYSv4Hg97O2qgD_SCfiEA', 'refresh_token': 'OAQABAAAAAACQN9QBRU3jT6bcBQLZNUj7n4mcjcIzVaUvvbK6qHWYOSzIZa_8f3hLGgC7BS1ARzfbjGUbUDnUrcLldj6KrRVDbRq24smHRMWciTrGCfoe6BqWM6Jewi4JHni5NnhVmqaTtVjVZtdD0KF_pT953lEbBDQDAJmMYlgHjd931vTTQH3EGu9sK0p7RQwciN2__zaYSBKE9AB_npoWWN85W5shszbuiZfklWq8HQNZQLuRix5xDTCv2F4Ng_0pegNk_UpNaxg4coM5jVd4LcvW2jaxg-1Vrdvtki6pfbf8hZBe0kaYBPDjrA3pa3OHtktRd8jE0aBfGSO07CR6204eAxxKz_qw7bSIzvr46tOl19TxP6DtxZTxbGmUag7W_NAt7fYH9FOBYuvE6K-DfuL4Ce4r2xckNiduya-MlIX7ZP2serivb86iStwv_t_zw0pnvXX9yGwmaWdgWBUF1VgsLhUDqqTbEcwxj7R5b1T6ZDOynJ0hVjvI09-ytJWTyC2MpIU5EXlEVph41iB3WY7tbSfIDGB4rZm-jOntdwqcqe3tEt38Ym1zd0BxO0SXCVa5eDs6rOT2Z1XQQaC6EemEmukdn_-m14DPdF15aXEVEJRzzM2GlYScMDdKuwsgo72G08LDdpuJZGWbVegZmUT3AgCmuMaXyuiJ3ozohOU-Jckf3AXeybKmuCEPedjeHJ3liS33_IfCl04nZFXtXB8UmQ2Jef8rM51PjobU7dKmLNdddGdejvWTgJ-gsUNi5UaKCSYgAA', 'id_token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IkJCOENlRlZxeWFHckdOdWVoSklpTDRkZmp6dyJ9.eyJhdWQiOiJiODRkYWU4Ny0yZjdhLTQzNzctODcwZC02MjQ3YTkxNjdhNmYiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vNjQ5NDQ2MGUtODYwMC00ZWRjLTg1MGYtNTI4ZThmYWFkMjkwL3YyLjAiLCJpYXQiOjE1NzY2ODg2NTIsIm5iZiI6MTU3NjY4ODY1MiwiZXhwIjoxNTc2NjkyNTUyLCJhaW8iOiJBVFFBeS84TkFBQUFObjRsYXB2ZURsUnBaUFNsOXg4WGU5K1RwUDlxcjV5bS80WnNJZnhvZTRRcEVHZ2RNdTdiZHlBTkFsaUJLRjNqIiwibmFtZSI6IlBvd2VyYmllbWJlZCIsIm9pZCI6IjUyZWZiYjlkLWJkMTMtNDAxZC04Y2ZiLTM1ZDFmODczOWE2MSIsInByZWZlcnJlZF91c2VybmFtZSI6InBvd2VyYmllbWJlZEBleHBlcnRpbWUuY29tIiwic3ViIjoiQTJMRjJLQ1k1SUx6RjM5a3dNUWlCWmlpeGhfdldaVEplNElLQWFJaEkxOCIsInRpZCI6IjY0OTQ0NjBlLTg2MDAtNGVkYy04NTBmLTUyOGU4ZmFhZDI5MCIsInV0aSI6Il9icE10aFgybWt5RU5OVHJ3TGNuQUEiLCJ2ZXIiOiIyLjAifQ.kz1eIBYT54bmMISH2cuyZF2GugRhEXFW42rzD89OIozRf-t6nMOwXNb6POhOw1F7kMedNi8izr7AnCd713BN7Fwojp0nRWKjJ-OXucdewRJHiSxkGW4mjGpxfEpuF5WpQlzjWUmsb74ijifIunQxfqIbr5_NuioMj1uldG1CHBzADK4-7sBOc0TZUTQsN1tS2o8V3Pj1LrmQRwIKgeKrY7vQoFFSWmg5QN_uqdnRASOBpkwEwmm5V4R_mkeR7ieTAidj0C2RgsHsqH4uf3jDmNNYu_asSa_Ls9v5UX9YDDeDuY25dPbZ74MptL_yfcPUGwwCUa3-ddfZ9jI_7unPcw'
# }
# # [ ] add RLS later
# graph_data = requests.post(  # Use token to call downstream service
#     config["endpoint"],
#     headers={
#         'Authorization': 'Bearer ' + result['access_token'],
#         'Content-Type': 'application/json'
#     },
#     json={
#         'accessLevel': 'View'
#     }).json()

# print("PBI embed token call result: %s" % json.dumps(graph_data, indent=2))
# exit()


# Optional logging
# logging.basicConfig(level=logging.DEBUG)  # Enable DEBUG log for entire script
# logging.getLogger("msal").setLevel(logging.INFO)  # Optionally disable MSAL DEBUG logs

config = json.load(open(sys.argv[1]))

# Create a preferably long-lived app instance which maintains a token cache.
app = msal.PublicClientApplication(
    config["client_id"], authority=config["authority"],
    # token_cache=...  # Default cache is in memory only.
    # You can learn how to use SerializableTokenCache from
    # https://msal-python.rtfd.io/en/latest/#msal.SerializableTokenCache
)

# The pattern to acquire a token looks like this.
result = None

# Firstly, check the cache to see if this end user has signed in before
accounts = app.get_accounts(username=config["username"])
if accounts:
    logging.info(
        "Account(s) exists in cache, probably with token too. Let's try.")
    result = app.acquire_token_silent(config["scope"], account=accounts[0])

if not result:
    logging.info(
        "No suitable token exists in cache. Let's get a new one from AAD.")
    # See this page for constraints of Username Password Flow.
    # https://github.com/AzureAD/microsoft-authentication-library-for-python/wiki/Username-Password-Authentication
    result = app.acquire_token_by_username_password(
        config["username"], config["password"], scopes=config["scope"])
    print('here')
if "access_token" in result:
    # print("PBI Access token call result: %s" % result["access_token"])
    # print(result)
    # Calling graph using the access token
    # graph_data = requests.get(  # Use token to call downstream service
    #     config["endpoint"],
    #     headers={'Authorization': 'Bearer ' + result['access_token']},).json()
    # print("Graph API call result: %s" % json.dumps(graph_data, indent=2))
    graph_data = requests.post(  # Use token to call downstream service
        config["endpoint"],
        headers={
            'Authorization': 'Bearer ' + result['access_token'],
            'Content-Type': 'application/json'
        },
        json={
            'accessLevel': 'View'
        }).json()
    print("Graph API call result: %s" % json.dumps(graph_data, indent=2))

    # should return this token to js code
    # print("PBI embed token call result: %s" % json.dumps(graph_data, indent=2))
else:
    print(result.get("error"))
    print(result.get("error_description"))
    # You may need this when reporting a bug
    print(result.get("correlation_id"))
    # Not mean to be coded programatically, but...
    if 65001 in result.get("error_codes", []):
        # AAD requires user consent for U/P flow
        print("Visit this to consent:",
        app.get_authorization_request_url(config["scope"]))
