/*
 *  Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license.
 *  See LICENSE in the source repository root for complete license information.
 */

var express = require('express');
var app = express();
var morgan = require('morgan');
var path = require('path');
const axios = require('axios');

// Initialize variables.
var port = 30662; // process.env.PORT || 30662;

// Configure morgan module to log all requests.
app.use(morgan('dev'));

// Set the front-end folder to serve public assets.
app.use(express.static('JavaScriptSPA'));

// Set up our one route to the index.html file.
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

// Set up the route for getting access token
app.get('/accessToken', async function(req, res) {
  // res.set('Content-Type', 'application/x-www-form-urlencoded');
  res.setHeader('Access-Control-Allow-Origin', '*');

  // const response = await getAccessToken();
  const response = await getAccessTokenTest();

  res.json({ accessToken: response });
});

/**
 * This function is called in node environment in order to get around CORS issues if called in a browser
 * @see https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth-ropc
 */
async function getAccessToken() {
  const bodyParameters = {
    grant_type: 'password',
    scope: 'openid',
    resource: 'https://analysis.windows.net/powerbi/api',
    client_id: 'b84dae87-2f7a-4377-870d-6247a9167a6f', // alexandre.hak@epitech.eu
    username: 'powerbiembed@expertime.com',
    password: 'Zuvo6377'
  };

  const token = await axios({
    method: 'post',
    url: 'https://login.microsoftonline.com/common/oauth2/token',
    withCredentials: true,
    crossdomain: true,
    data: Object.keys(bodyParameters)
      .map(function(key) {
        return (
          encodeURIComponent(key) +
          '=' +
          encodeURIComponent(bodyParameters[key])
        );
      })
      .join('&'),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
    .then(function(response) {
      bearerToken = response.data.access_token;
      console.log(bearerToken);
      return bearerToken;
    })
    .catch(function(error) {
      console.log(error);
    });

  return token;
}

async function getAccessTokenTest() {
  const url = 'https://login.microsoftonline.com/common/oauth2/token';

  let data = new URLSearchParams();
  data.append('grant_type', 'password');
  data.append('scope', 'https://analysis.windows.net/powerbi/api/.default');
  data.append('resource', 'https://analysis.windows.net/powerbi/api');
  data.append('client_id', PBIClientId);
  data.append('username', PBIUsername);
  data.append('password', PBIPassword);

  return (
    fetch(url, {
      method: 'POST',
      credentials: 'include',
      mode: 'no-cors',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Methods': 'POST,GET,HEAD,OPTIONS,PUT,DELETE',
        Accept: '*/*',
      },
      body: data,
    })
      //.then(checkStatus)
      .then(parseJSON)
  );
}

// Start the server.
app.listen(port);
console.log('Listening on port ' + port + '...');
